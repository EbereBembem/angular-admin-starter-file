import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';
import { OnlineGuard } from './core/guards/online.guard';
import { AppLayoutComponent } from './layouts/app-layout/app-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { LayoutsModule } from './layouts/layouts.module';
import { UserLayoutComponent } from './layouts/user-layout/user-layout.component';
import { OfflinePageComponent } from './pages/home/offline-page/offline-page.component';

const DEFAULT_REDIRECT_ROUTE = '/users';

const routes: Routes = [
  {
    path: '',
    component: AppLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'users',
        loadChildren: () =>
          import('./pages/users/users.module').then((mod) => mod.UsersModule),
      },
      // {
      //   path: 'category',
      //   loadChildren: () =>
      //     import('./pages/category/category.module').then(
      //       (mod) => mod.CategoryModule
      //     ),
      // },
      // {
      //   path: 'sub-category',
      //   loadChildren: () =>
      //     import('./pages/sub-category/sub-category.module').then(
      //       (mod) => mod.SubCategoryModule
      //     ),
      // },
      // {
      //   path: 'ad-category',
      //   loadChildren: () =>
      //     import('./pages/ad-category/ad-category.module').then(
      //       (mod) => mod.AdCategoryModule
      //     ),
      // },
      // {
      //   path: 'adverts',
      //   loadChildren: () =>
      //     import('./pages/adverts/adverts.module').then(
      //       (mod) => mod.AdvertsModule
      //     ),
      // },
      // {
      //   path: 'products',
      //   loadChildren: () =>
      //     import('./pages/products/products.module').then(
      //       (mod) => mod.ProductsModule
      //     ),
      // },
      // {
      //   path: 'orders',
      //   loadChildren: () =>
      //     import('./pages/orders/orders.module').then(
      //       (mod) => mod.OrdersModule
      //     ),
      // },
      // {
      //   path: 'transactions',
      //   loadChildren: () =>
      //     import('./pages/transactions/transactions.module').then(
      //       (mod) => mod.TransactionsModule
      //     ),
      // },
      // {
      //   path: 'disputes',
      //   loadChildren: () =>
      //     import('./pages/disputes/disputes.module').then(
      //       (mod) => mod.DisputesModule
      //     ),
      // },
      // {
      //   path: 'my-account',
      //   canActivate: [OnlyAdminGuard],
      //   loadChildren: () =>
      //     import('./pages/admin-account/admin-account.module').then(
      //       (mod) => mod.AdminAccountModule
      //     ),
      // },
      // {
      //   path: '',
      //   canActivate: [OnlySuperAdminGuard],
      //   children: [
      //     {
      //       path: 'admin',
      //       loadChildren: () =>
      //         import('./pages/admins/admins.module').then(
      //           (mod) => mod.AdminsModule
      //         ),
      //     },
      //     {
      //       path: 'delivery-agents',
      //       loadChildren: () =>
      //         import('./pages/delivery-agents/delivery-agents.module').then(
      //           (mod) => mod.DeliveryAgentsModule
      //         ),
      //     },
      //     {
      //       path: 'app-config',
      //       loadChildren: () =>
      //         import('./pages/app-config/app-config.module').then(
      //           (mod) => mod.AppConfigModule
      //         ),
      //     },
      //     {
      //       path: 'reports',
      //       loadChildren: () =>
      //         import('./pages/reports/reports.module').then(
      //           (mod) => mod.ReportsModule
      //         ),
      //     },
      //     {
      //       path: '',
      //       redirectTo: '/users/users-table/all-users',
      //       pathMatch: 'full',
      //     },
      //   ],
      // },
      {
        path: '',
        redirectTo: DEFAULT_REDIRECT_ROUTE,
        pathMatch: 'full',
      },
    ],
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/auth/auth.module').then((mod) => mod.AuthModule),
  },
  {
    path: '',
    redirectTo: DEFAULT_REDIRECT_ROUTE,
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: DEFAULT_REDIRECT_ROUTE,
  },
];
@NgModule({
  imports: [
    LayoutsModule,
    RouterModule.forRoot(routes, {
      relativeLinkResolution: 'legacy',
      scrollPositionRestoration: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
