import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { HOME_ROUTES } from './home.routes';
import { HomeComponent } from './home/home.component';
import { OfflinePageComponent } from './offline-page/offline-page.component';

@NgModule({
  declarations: [HomeComponent, OfflinePageComponent],
  imports: [SharedModule, RouterModule.forChild(HOME_ROUTES)],
})
export class HomeModule {}
