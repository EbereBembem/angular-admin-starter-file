import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { fromEvent, Observable, of, Subscription } from 'rxjs';
import { AlertService } from 'src/app/core/services/alert.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-offline-page',
  templateUrl: './offline-page.component.html',
  styleUrls: ['./offline-page.component.scss'],
})
export class OfflinePageComponent implements OnInit, OnDestroy {
  subscriptions = new Subscription();
  url = '/';

  onlineEvent: Observable<Event> = of();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private alert: AlertService
  ) {}

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.url = this.route?.snapshot?.queryParams?.redirectUrl || '/';

    this.onlineEvent = fromEvent(window, 'online');

    this.subscriptions.add(
      this.onlineEvent.subscribe(() => {
        this.alert.showSuccess(environment.onlineMsg, {
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
        });
        this.router.navigateByUrl(this.url);
      })
    );
  }
}
