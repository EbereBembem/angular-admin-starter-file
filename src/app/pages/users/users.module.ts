import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserTablesComponent } from './user-tables/user-tables.component';



@NgModule({
  declarations: [
    UserTablesComponent
  ],
  imports: [
    CommonModule
  ]
})
export class UsersModule { }
