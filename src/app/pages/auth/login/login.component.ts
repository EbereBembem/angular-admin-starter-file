import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { USER_DASHBOARD_ROUTE } from 'src/app/core/constants/app-routes';
import { DEFAULT_ERROR_MSG } from 'src/app/core/constants/text-utils';
import { AuthenticationService } from 'src/app/core/security/authentication.service';
import { AlertService } from 'src/app/core/services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  subscription = new Subscription();
  isLoading = false;
  url_route!: any;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private auth: AuthenticationService,
    private alertService: AlertService
  ) {}

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.subscription.add(
      this.route.queryParams.subscribe((queryParams) => {
        this.url_route =
          queryParams &&
          queryParams.redirectUrl &&
          queryParams.redirectUrl !== '/'
            ? queryParams.redirectUrl
            : USER_DASHBOARD_ROUTE;
      })
    );

    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  get formControls() {
    return this.loginForm.controls;
  }

  login() {
    if (this.loginForm.invalid) {
      return;
    }
    const userEmail = this.loginForm.value.email;
    const userPassword = this.loginForm.value.password;
    this.isLoading = true;

    this.subscription.add(
      this.auth.login(userEmail, userPassword).subscribe(
        (resp: any) => {
          this.isLoading = false;
          this.router.navigate([this.url_route]);
        },
        (err: HttpErrorResponse) => {
          this.isLoading = false;
          const errorMessage: string = err?.error?.message || DEFAULT_ERROR_MSG;
          this.alertService.showError(errorMessage);
        }
      )
    );
  }
}
