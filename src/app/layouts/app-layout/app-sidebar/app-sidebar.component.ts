import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NAV_LIST } from 'src/app/core/constants/navs';
import { IAdmin } from 'src/app/core/models/admin';
import { AuthenticationService } from 'src/app/core/security/authentication.service';
import { MenuService } from 'src/app/core/services/menu.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './app-sidebar.component.html',
  styleUrls: ['./app-sidebar.component.scss'],
})
export class AppSidebarComponent implements OnInit {
  menuList = NAV_LIST;
  selectedMenu!: string | null;
  userObs!: Observable<IAdmin>;

  constructor(
    private menu: MenuService,
    private auth: AuthenticationService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    // Subscribe to user type
    this.userObs = combineLatest([
      this.menu.selectedMenu$,
      this.auth.initCurrentUser(),
    ]).pipe(
      map(([obsSelectedMenu, admin]) => {
        if (obsSelectedMenu) {
          this.selectedMenu = obsSelectedMenu;
        }
        return admin;
      })
    );

    // USE THE SAME FN TO GET THE CORRECT SELECTEDMENU ON iNIT WITH location.path()

    this.route.url.subscribe((url) => {
      const path = window.location.pathname;
      this.findSelectedMenu(path);
    });
  }

  setSelectedMenu(value: string) {
    this.selectedMenu = this.selectedMenu === value ? null : value;
  }

  private findSelectedMenu(url: string) {
    for (let index = 0; index < NAV_LIST.length; index++) {
      const element = NAV_LIST[index];
      if (element.children?.length) {
        const children = element.children.map((child) => child.link);
        const childRegx = new RegExp(children.join('|'));
        if (childRegx.test(url)) {
          this.setSelectedMenu(element.id);
          break;
        }
      }
    }
  }
}
