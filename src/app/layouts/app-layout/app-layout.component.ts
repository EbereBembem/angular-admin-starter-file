import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { NAV_LIST } from 'src/app/core/constants/navs';
import { USER_DATA } from 'src/app/core/mocks/user';
import { IAdmin } from 'src/app/core/models/admin';
import { IMenu } from 'src/app/core/models/menu';
import { AuthenticationService } from 'src/app/core/security/authentication.service';
import { MenuService } from 'src/app/core/services/menu.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.scss'],
})
export class AppLayoutComponent implements OnInit {
  @ViewChild('sidenav') sidenav: MatSidenav | undefined;
  mobileQuery: MediaQueryList;
  open = false;
  user$!: Observable<IAdmin | null>;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private menuService: MenuService,
    private authService: AuthenticationService,
    private router: Router
  ) {
    this.mobileQuery = media.matchMedia('(min-width: 768px)');
    this.mobileQueryListener = () => {
      if (this.mobileQuery.matches) {
        this.menuService.setShowSidebar(false);
      }
      return changeDetectorRef.detectChanges();
    };
    this.mobileQuery.addEventListener('change', this.mobileQueryListener);
  }

  private mobileQueryListener: () => void;

  ngOnInit(): void {
    // this.user$ = this.authService.currentUser$;
    this.user$ = of(USER_DATA);
  }

  handleToggle(action: boolean) {
    action ? this.handleOpen() : this.handleClose();
  }

  handleOpen() {
    (this.sidenav as MatSidenav).open();
  }

  handleClose() {
    (this.sidenav as MatSidenav).close();
  }
}
