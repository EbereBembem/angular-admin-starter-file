import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { IAdmin } from 'src/app/core/models/admin';
import { AuthenticationService } from 'src/app/core/security/authentication.service';
import { UserService } from 'src/app/core/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  @Output() openMenu = new EventEmitter<boolean>();
  @Input() user!: IAdmin;
  isOpen = false;
  subscriptions = new Subscription();

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private userService: UserService
  ) {}

  ngOnInit() {}

  logout() {
    this.authService.logOut();
    this.authService.setCurrentUser(null);
  }

  open() {
    this.isOpen = !this.isOpen;
    this.openMenu.emit(this.isOpen);
  }

  preview() {
    window.open('url', '_blank');
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
