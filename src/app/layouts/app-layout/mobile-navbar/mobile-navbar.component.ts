import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { SIGNUP_ROUTE, LOGIN_ROUTE } from 'src/app/core/constants/app-routes';
import { NAV_LIST } from 'src/app/core/constants/navs';
import { IMenu } from 'src/app/core/models/menu';
import { IUser } from 'src/app/core/models/user';

@Component({
  selector: 'app-mobile-navbar',
  templateUrl: './mobile-navbar.component.html',
  styleUrls: ['./mobile-navbar.component.scss'],
})
export class MobileNavbarComponent implements OnInit {
  @Output() closeMenu = new EventEmitter<boolean>();
  navList = NAV_LIST;

  readonly signupRoute = SIGNUP_ROUTE;
  readonly loginRoute = LOGIN_ROUTE;
  readonly userAccountRoute = '';
  constructor(private router: Router) {}

  user$: Observable<IUser> = of();
  selectedIndex!: number | null;

  ngOnInit(): void {}

  stop(event: any) {
    event.preventDefault();
    event.stopPropagation();
  }

  close() {
    this.closeMenu.emit(false);
  }

  gotoSignUpRoute() {
    this.gotoRoute(this.signupRoute);
  }

  gotoLoginRoute() {
    this.gotoRoute(this.loginRoute);
  }

  logout() {
    // this.auth.logOut();
  }

  gotoRoute(route: string) {
    this.router.navigateByUrl(route);
    this.close();
  }

  menuClick(menu: IMenu, index = null) {
    if (menu?.link) {
      this.gotoRoute(menu.link);
    }
    this.selectedIndex = index;
  }
}
