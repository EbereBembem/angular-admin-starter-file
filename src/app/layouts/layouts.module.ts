import { NgModule } from '@angular/core';
import { ScrollModule } from '../components/scroll/scroll.module';
import { SharedModule } from '../shared/shared.module';
import { AppLayoutComponent } from './app-layout/app-layout.component';
import { FooterComponent } from './app-layout/footer/footer.component';
import { MobileNavbarComponent } from './app-layout/mobile-navbar/mobile-navbar.component';
import { NavbarComponent } from './app-layout/navbar/navbar.component';
import { AuthLayoutComponent } from './auth-layout/auth-layout.component';
import { UserLayoutComponent } from './user-layout/user-layout.component';
import { AppSidebarComponent } from './app-layout/app-sidebar/app-sidebar.component';

@NgModule({
  declarations: [
    AppLayoutComponent,
    MobileNavbarComponent,
    NavbarComponent,
    FooterComponent,
    AuthLayoutComponent,
    UserLayoutComponent,
    AppSidebarComponent,
  ],
  imports: [SharedModule, ScrollModule],
})
export class LayoutsModule {}
