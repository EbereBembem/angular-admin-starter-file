import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AlertService } from '../services/alert.service';

@Injectable({
  providedIn: 'root',
})
export class OnlineGuard implements CanActivate, CanActivateChild {
  constructor(private router: Router, private alert: AlertService) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (!navigator.onLine) {
      this.alert.showError(environment.offlineMsg, {
        verticalPosition: 'bottom',
        horizontalPosition: 'center',
      });
      return true;
    }
    return this.router.parseUrl('/');
  }
  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (navigator.onLine) {
      return true;
    }
    const redirectUrl = (childRoute as any)['_routerState']['url'];

    this.router.navigateByUrl(
      this.router.createUrlTree(['/offline'], {
        queryParams: {
          redirectUrl,
        },
      })
    );
    this.alert.showError(environment.offlineMsg, {
      verticalPosition: 'bottom',
      horizontalPosition: 'center',
    });
    return false;
  }
}
