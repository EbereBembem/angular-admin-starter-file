import { TestBed } from '@angular/core/testing';

import { OnlySuperAdminGuard } from './only-super-admin.guard';

describe('OnlySuperAdminGuard', () => {
  let guard: OnlySuperAdminGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(OnlySuperAdminGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
