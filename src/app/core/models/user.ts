export type UserGenderType = 'male' | 'female';

export interface IUser {
  uid?: string;
  isDelete?: boolean;
  isBlocked?: boolean;
  firstName?: string;
  lastName?: string;
  email?: string;
  phone?: string;
  gender?: UserGenderType;
  emailVerified?: boolean;
  createdAt?: string;
  updatedAt?: string;
}
