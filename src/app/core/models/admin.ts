export interface IAdmin {
  id?: number;
  createdAt?: number;
  firstName?: string;
  gender?: string;
  isAdmin?: boolean;
  name?: string;
  emailAddress?: string;
  email?: string;
  isSuperAdmin?: boolean;
  lastName?: string;
  lastSeenAt?: number;
  phoneNumber?: string;
  phone?: string;
  profileImage?: string;
  tosAcceptedByIp?: string;
  updatedAt?: number;
  isChecked?: boolean;
  blockedReason?: string;
  blockedBy?: IAdmin;
  isBlocked?: boolean;
  token?: string;
  password?: string;
}
