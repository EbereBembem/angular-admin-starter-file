export interface IMenu {
  id: string;
  name: string;
  link: string;
  active?: boolean;
  icon?: string;
  children?: IMenu[];
  isSuperAdminOnly?: boolean;
  isAdminOnly?: boolean;
}
