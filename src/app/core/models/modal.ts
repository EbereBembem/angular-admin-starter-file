export interface IModalConfirmationData {
  content: string;
  primaryActionText?: string;
  cancelActionText?: string;
}
