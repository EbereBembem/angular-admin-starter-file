export type AlertType = 'success' | 'warning' | 'info' | 'error';

export interface IAlertContent {
  message: string;
  type: AlertType;
  matIcon?: string;
}
