import { AbstractControl, FormGroup } from '@angular/forms';

export function resetFormControls(formControls: {
  [key: string]: AbstractControl;
}) {
  if (formControls) {
    Object.keys(formControls).forEach((key) => {
      const control = formControls[key];
      control.reset();
      control.setErrors(null);
      if (control instanceof FormGroup) {
        resetFormControls((<FormGroup>control).controls);
      }
    });
  }
}
