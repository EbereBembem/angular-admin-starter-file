export default function listMapper({
  key,
  list,
}: {
  key: string;
  list: any[];
}) {
  let mapObj: any = {};
  if (list.length) {
    list.forEach((item) => {
      mapObj[item[key]] = item;
    });
  }
  return mapObj;
}

export function getListTitleMapper(list: any, id: string, subId: string) {
  if (list && list[id]) {
    const idChildren: any[] = list[id].children;
    const result = idChildren.find((item) => item.id === subId);

    if (result && result.name) {
      return result.name;
    }
  }
  return;
}
