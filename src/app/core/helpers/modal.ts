import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

export function openModal(
  dialog: MatDialog,
  component: any,
  modalSettings: MatDialogConfig,
  callback?: Function
) {
  const subscription = new Subscription();
  const dialogRef = dialog.open(component, modalSettings);

  if (callback) {
    subscription.add(
      dialogRef.afterClosed().subscribe((res) => {
        callback(res);
        subscription.unsubscribe();
      })
    );
  }
}
