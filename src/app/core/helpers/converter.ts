import { FOR_REMOVE_COMMAS } from '../constants/regex';

export function convertCurrnecyFormattedToNumber(
  value: string | number
): number {
  if ((value || +value === 0) && (value !== null || value !== undefined)) {
    const result = +value.toString().replace(FOR_REMOVE_COMMAS, '');
    return result;
  }
  return 0;
}

export function amountToOurMoneyValue(value: number | string): number {
  const amt = convertCurrnecyFormattedToNumber(value.toString());
  if ((amt || +amt === 0) && !isNaN(+amt)) {
    return +amt * 100;
  }
  return 0;
}

export function hoursDurationToOurMillisecond(value: number): number {
  if ((value || +value === 0) && !isNaN(+value)) {
    return +value * 60 * 60 * 1000;
  }
  return 0;
}

export function millisecondToHoursDuration(value: number): number {
  if ((value || +value === 0) && !isNaN(+value)) {
    return +value / (60 * 60 * 1000);
  }
  return 0;
}

export function convertToCSV(data: any[]) {
  const replacer = (key: any, value: any) => (value === null ? '' : value); // specify how you want to handle null values here
  const header = Object.keys(data[0]);
  const csv = [
    header.join(','), // header row first
    ...data.map((row) =>
      header
        .map((fieldName) => JSON.stringify(row[fieldName], replacer))
        .join(',')
    ),
  ].join('\r\n');

  return `data:text/csv;charset=utf-8,${csv}`;
}

export const capitalizeWord = (word: string): string => {
  if (word) {
    return `${word.charAt(0).toUpperCase() || ''}${word.substring(1) || ''}`;
  }
  return word;
};

export const capitalizeSentence = (sentence: string): string => {
  if (sentence) {
    return sentence
      .toLowerCase()
      .split(' ')
      .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
      .join(' ');
  }
  return sentence;
};
