import { NON_TOKEN_ENDPOINT_PATHS } from '../constants/api';

export function isTokenURL(url: string) {
  return !NON_TOKEN_ENDPOINT_PATHS.some(
    (endpoint) => url.indexOf(endpoint) >= 0
  );
}
