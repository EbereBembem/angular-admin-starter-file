import { HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, retryWhen } from 'rxjs/operators';

const DEFAULT_MAX_RETRIES = 3;
const DEFAULT_BACKOFF = 5000;

//retryAfter specifies the delay in milliseconds between each retry.
// maxRetry is the number os times to retry
// delayAfter Retry is the increment number of time to delay before retying
// The first retry happens after one second, the second one after two seconds and the third one after three seconds.
export function retryWithBackoff(
  retryAfter: number,
  maxRetry = DEFAULT_MAX_RETRIES,
  delayAfterRetry = DEFAULT_BACKOFF
) {
  let retries = maxRetry;

  return (src: Observable<any>) => {
    return src.pipe(
      retryWhen((errors: Observable<any>) =>
        errors.pipe(
          mergeMap((error) => {
            if (retries-- > 0) {
              const backoffTime =
                retryAfter + (maxRetry - retries) * delayAfterRetry;
              return of(error).pipe(delay(backoffTime));
            }
            return throwError('Request Failed');
          })
        )
      )
    );
  };
}

export function errorResponseConverter(err: HttpErrorResponse) {
  const result = (err && err.error) || err;
  return of(result);
}
