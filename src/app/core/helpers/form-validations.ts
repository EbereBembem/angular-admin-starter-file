import {
  AbstractControl,
  FormControl,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';

export function uniqueFormFieldValue(
  control: AbstractControl,
  formArray: any[],
  key = 'name',
  isString = true
): ValidationErrors | null {
  if (formArray && formArray.length && control) {
    const listOfValues = formArray.filter((value) => {
      if (value && value[key] && value[key] !== null) {
        if (isString) {
          if (
            (value[key] as string).toLowerCase() ===
            (control.value as string).toLowerCase()
          ) {
            return value[key];
          }
        } else if (value[key] == control.value) {
          return value[key];
        }
      }
    });

    const result: any = {};
    result[`${key}NotUnique`] = true;

    return listOfValues.length ? { ...result } : null;
  }
  return null;
}

export function minGreaterThanZero(
  control: AbstractControl
): ValidationErrors | null {
  if (control && Number(control.value) >= 0) {
    const result = Number(control.value);
    return result > 0 ? null : { min: true };
  }
  return null;
}

export function minimumNumber(min: number): ValidatorFn | any {
  return (control: FormControl): { [key: string]: boolean } | null => {
    let val: number = control.value;

    if (control.pristine || control.pristine) {
      return null;
    }
    if (val >= min) {
      return null;
    }
    return { min: true };
  };
}

export function mustBeWholeNumber(
  control: AbstractControl
): ValidationErrors | null {
  if (control && Number(control.value) >= 0) {
    const regex = new RegExp(/^[0-9]\d*$/g);
    const result = Number(control.value).toString();
    return regex.test(result) ? null : { notWholeNumber: true };
  }
  return null;
}
