import * as moment from 'moment';

export function helperExample() {
  alert('This is an example');
}

export function downloadCSV(csv: string, filename: string) {
  const encodedUri = encodeURI(csv);
  const link = document.createElement('a');
  link.setAttribute('href', encodedUri);
  link.setAttribute(
    'download',
    `${filename}_${moment().format('DD_MM_YYYY')}.csv`
  );
  document.body.appendChild(link); // Required for FF

  link.click(); // This will download the data file named "my_data.csv".
}
