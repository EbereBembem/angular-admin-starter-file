import { IModalConfirmationData } from '../models/modal';

export const CONFFIRMATION_POPUP_MODAL_DATA: IModalConfirmationData = {
  content: 'Are you sure you want to delete this address?',
  primaryActionText: 'Yes, Delete',
};

export const SEND_CORPRATE_POPUP_MODAL_DATA: IModalConfirmationData = {
  content: 'Are you sure you want to delete this address?',
  primaryActionText: 'Yes, Delete',
};

export const DEFAULT_CONFIRM_POPUP_MODAL_DATA: IModalConfirmationData = {
  content: 'Are you sure you want to proceed with this action?',
  primaryActionText: 'Yes',
  cancelActionText: 'No',
};
