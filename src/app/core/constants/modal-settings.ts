import { MatDialogConfig } from '@angular/material/dialog';

export const DEFAULT_MODAL_SETTING: MatDialogConfig = {
  width: '100%',
  disableClose: false,
  hasBackdrop: true,
};

export const CONFFIRMATION_POPUP_MODAL_SETTING: MatDialogConfig = {
  ...DEFAULT_MODAL_SETTING,
  panelClass: 'custom-confirmation-modal',
};

export const SEND_CORPRATE_ENQUIRY_POPUP_MODAL_SETTING: MatDialogConfig = {
  ...DEFAULT_MODAL_SETTING,
  panelClass: 'send-corporate-email-modal',
  disableClose: true,
};

export const IMAGE_SLIDER_PREVIEW_MODAL_SETTING: MatDialogConfig = {
  ...DEFAULT_MODAL_SETTING,
  panelClass: 'preview-image-slider-modal',
};
