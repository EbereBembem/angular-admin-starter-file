import { environment } from 'src/environments/environment';

export const NO_TOKEN_ENDPOINT_PATHS = [];
const BASE_API = environment.baseAPI;
export const API = {
  AUTH: {
    LOGIN: '',
    IAM: '',
  },
  USER: {
    GET_USER: `${BASE_API}`,
  },
};

export const NON_TOKEN_ENDPOINT_PATHS = [API.AUTH.LOGIN];
