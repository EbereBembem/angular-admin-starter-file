import { IMenu } from '../models/menu';

export const NAV_LIST: IMenu[] = [
  {
    name: 'home',
    id: '5',
    link: '',
    active: true,
    icon: 'group',
  },
];
