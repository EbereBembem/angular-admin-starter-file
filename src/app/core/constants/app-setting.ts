export const APP_NAME = 'app';
export const TOKEN = `${APP_NAME.toUpperCase()}_AUTH_TOKEN`;
export const SALT = `${APP_NAME}-x35&#`;
export const POLISH = `${APP_NAME}-admin-spt`;
export const USER_SALT = `${APP_NAME}-admin-user`;
export const POLISHUSER = `${POLISH}-${USER_SALT}`;
export const IS_ENCRYPTED = true;
