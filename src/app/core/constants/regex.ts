export const AMT_ONLY_REGEX: RegExp = RegExp(/[^\d.-]/g);
export const FOR_REMOVE_COMMAS: RegExp = RegExp(/[,\s]/gi);
export const TWO_DECIMAL_PLACE: RegExp = RegExp(/^[0-9]*\.[0-9]{2}$/g);
export const HAS_DECIMAL: RegExp = RegExp(/[.]/g);
export const CONTAINS_NUMBER: RegExp = RegExp(/[\D_]+/g);
