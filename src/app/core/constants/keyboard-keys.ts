export const KEYBOARD_BACKSPACE = 'Backspace';
export const KEYBOARD_DELETE = 'Delete';
export const KEYBOARD_ARROW_UP = 'ArrowUp';
export const KEYBOARD_ARROW_DOWN = 'ArrowDown';
export const KEYBOARD_ARROW_RIGHT = 'ArrowRight';
export const KEYBOARD_ARROW_LEFT = 'ArrowLeft';
export const KEYBOARD_COMMA = ',';
export const KEYBOARD_PERIOD = '.';
export const KEYBOARD_EVENT_KEYS = [
  KEYBOARD_BACKSPACE,
  KEYBOARD_DELETE,
  KEYBOARD_ARROW_UP,
  KEYBOARD_ARROW_DOWN,
  KEYBOARD_ARROW_RIGHT,
  KEYBOARD_ARROW_LEFT,
  KEYBOARD_PERIOD,
  'Control',
  'Tab',
  'Shift',
  'Enter',
];
export const KEYBOARD_AMOUNT_EVENT_KEYS = [KEYBOARD_COMMA, KEYBOARD_PERIOD];
