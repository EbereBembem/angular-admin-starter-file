import { Injectable } from '@angular/core';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  Router,
} from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { API } from '../constants/api';
import {
  SALT,
  POLISH,
  POLISHUSER,
  TOKEN,
  USER_SALT,
  IS_ENCRYPTED,
} from '../constants/app-setting';
import { ApiService } from '../services/api.service';
import { CrispyService } from '../services/crispy.service';
import { IAdmin } from './../models/admin';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private readonly salt = SALT;
  private readonly polish = POLISH;
  private readonly polishUser = POLISHUSER;
  private readonly isEncrypted = IS_ENCRYPTED;
  private route: ActivatedRouteSnapshot;

  private signedInAdmin = new BehaviorSubject<IAdmin | null>(null);
  currentUser$ = this.signedInAdmin.asObservable();

  constructor(
    private router: Router,
    private crispyService: CrispyService,
    private activatedRoute: ActivatedRoute,
    private apiService: ApiService
  ) {
    this.route = activatedRoute.snapshot;
  }

  login(email: string, password: string): Observable<any> {
    return this.apiService
      .post<any>(API.AUTH.LOGIN, {
        emailAddress: email,
        password,
      })
      .pipe(
        tap((resp) => {
          if (resp.success) {
            this.setToken(resp.token, email);
            this.setCurrentUser(resp.admin);
            resp.token = '';
          }
        })
      );
  }

  initCurrentUser() {
    return of(this.getCurrentUser()).pipe(
      tap((admin) => {
        if (admin) {
          this.signedInAdmin.next(admin);
        }
      })
    );
  }

  appInitialize() {
    return this.getMyInfo()
      .toPromise()
      .catch(() => null);
  }

  private getMyInfo(): Observable<any> {
    return of(this.getToken()).pipe(
      switchMap(({ token }) => {
        if (token) {
          return this.apiService.get(API.AUTH.IAM) as Observable<any>;
        }
        return of(null);
      }),
      map((data: any | null) => {
        if (data && data.user) {
          const user = data.user;
          user.token = '';
          this.setCurrentUser(user);
          return user;
        }
        return null;
      }),
      tap((value) => this.setCurrentUser(value))
    );
  }

  getCurrentAdmin(): IAdmin | any {
    return this.getCurrentUser();
  }

  setCurrentUser(user: IAdmin | any): void {
    this.setAdmin(user);
  }

  setToken(token: string, emailLog: string): void {
    this.setUserToken(emailLog, token);
  }

  getToken() {
    let reqToken = { token: '' };

    let userToken = '';

    if (this.isEncrypted) {
      const user = localStorage.getItem(this.polish);
      const tk = localStorage.getItem(user as string);

      if (user && tk) {
        userToken = this.crispyService.decryptyCrypto(tk, this.salt + user);
      }
    } else {
      userToken = localStorage.getItem(TOKEN) || '';
    }
    reqToken.token = userToken;
    return reqToken;
  }

  isLogged(): boolean {
    const reqtoken = this.getToken();
    return !!(reqtoken && reqtoken.token);
  }

  logOut(shouldRedirect?: boolean): void {
    this.removeUser();
    this.removeUserToken();
    localStorage.clear();
    if (shouldRedirect) {
      const redirectUrl = (this.route as any)['_routerState']['url'];
      this.router.navigateByUrl(
        this.router.createUrlTree(['/login'], {
          queryParams: {
            redirectUrl,
          },
        })
      );
    } else {
      this.router.navigate(['/login']);
    }
  }

  private getCurrentUser(): IAdmin | any {
    if (this.isEncrypted) {
      const userE = localStorage.getItem(this.polishUser);
      const pU = localStorage.getItem(userE as string);
      if (userE && pU) {
        const leUser: string = this.crispyService.decryptyCrypto(
          pU,
          this.salt + userE
        );
        let admin = JSON.parse(leUser) as IAdmin;
        admin = this.removerUserValues(admin);

        return admin;
      }
      return;
    } else {
      return this.signedInAdmin.value;
    }
  }

  private setAdmin(user: IAdmin) {
    if (user) {
      if (this.isEncrypted) {
        const userEmail = this.crispyService.encryptyCrypto(
          user.email,
          this.salt + TOKEN + USER_SALT
        );

        user = this.removerUserValues(user);

        const leUser = this.crispyService.encryptyCrypto(
          JSON.stringify(user),
          this.salt + userEmail
        );

        localStorage.setItem(userEmail, leUser);
        localStorage.setItem(this.polishUser, userEmail);
      }
      this.signedInAdmin.next(user);
    }
  }

  private setUserToken(emailLog: string, token: string) {
    if (this.isEncrypted) {
      const user = this.crispyService.encryptyCrypto(
        emailLog,
        this.salt + TOKEN
      );
      const userToken = this.crispyService.encryptyCrypto(
        token,
        this.salt + user
      );

      localStorage.setItem(user, userToken);
      localStorage.setItem(this.polish, user);
    } else {
      localStorage.setItem(TOKEN, token);
    }
  }

  private removeUserToken(): void {
    this.removeUserPropertyFromStorage(this.polish);
  }

  private removeUser(): void {
    this.removeUserPropertyFromStorage(this.polishUser);
  }

  private removeUserPropertyFromStorage(key: string) {
    const keyValue = localStorage.getItem(key);
    localStorage.removeItem(key);
    if (keyValue) {
      localStorage.removeItem(keyValue);
    }
  }

  private removerUserValues(user: IAdmin) {
    if (user?.token) {
      delete user.token;
    }

    if (user?.password) {
      delete user.password;
    }

    return user;
  }
}
