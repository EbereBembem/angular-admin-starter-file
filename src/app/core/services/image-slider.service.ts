import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ImageSliderService {
  private imageSliderBehSubj = new BehaviorSubject<string[]>([]);
  imageSlider$ = this.imageSliderBehSubj.asObservable();

  private imageSliderIndexBehSubj = new BehaviorSubject<number>(0);
  imageSliderIndex$ = this.imageSliderIndexBehSubj.asObservable();

  constructor() {}

  setImageSliders(images: string[]) {
    this.imageSliderBehSubj.next(images);
  }

  setSelectedIndex(index: number) {
    this.imageSliderIndexBehSubj.next(index);
  }
}
