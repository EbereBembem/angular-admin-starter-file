import { TestBed } from '@angular/core/testing';

import { CrispyService } from './crispy.service';

describe('CrispyService', () => {
  let service: CrispyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrispyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
