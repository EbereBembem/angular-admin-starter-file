import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { CustomAlertComponent } from 'src/app/shared/components/custom-alert/custom-alert.component';
import { CustomConfirmationModalComponent } from 'src/app/shared/components/custom-confirmation-modal/custom-confirmation-modal.component';
import { DEFAULT_CONFIRM_POPUP_MODAL_DATA } from '../constants/modal-data';
import { CONFFIRMATION_POPUP_MODAL_SETTING } from '../constants/modal-settings';
import { AlertType, IAlertContent } from '../models/alert';
import { IModalConfirmationData } from '../models/modal';

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  private readonly duration = 6000;

  constructor(public snackBar: MatSnackBar, public dialog: MatDialog) {}
  showSuccess(message: string, options?: MatSnackBarConfig) {
    return this.alert('success', message, options);
  }

  showError(message: string, options?: MatSnackBarConfig) {
    return this.alert('error', message, options);
  }

  showInfo(message: string, options?: MatSnackBarConfig) {
    return this.alert('info', message, options);
  }

  showWarning(message: string, options?: MatSnackBarConfig) {
    return this.alert('warning', message, options);
  }

  showConfirmationModal(option?: IModalConfirmationData) {
    const data = {
      ...DEFAULT_CONFIRM_POPUP_MODAL_DATA,
      ...option,
    };
    const dialogRef = this.dialog.open(CustomConfirmationModalComponent, {
      ...CONFFIRMATION_POPUP_MODAL_SETTING,
      data,
    });
    return dialogRef.afterClosed();
  }

  basicAlert(message: string, title: string, options: MatSnackBarConfig = {}) {
    return this.snackBar.open(message, title, {
      duration: this.duration,
      ...options,
    });
  }

  private alert(
    type: AlertType,
    message: string,
    options: MatSnackBarConfig = {}
  ) {
    const data: IAlertContent = {
      message,
      type,
    };
    this.snackBar.openFromComponent(CustomAlertComponent, {
      panelClass: 'custom-alert-snackbar',
      verticalPosition: 'top',
      horizontalPosition: 'right',
      data,
      ...options,
    });
  }
}
