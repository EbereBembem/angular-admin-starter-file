import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IMenu } from '../models/menu';

@Injectable({
  providedIn: 'root',
})
export class MenuService {
  private showSidebar = new BehaviorSubject<boolean>(false);
  showSideBar$ = this.showSidebar.asObservable();
  private contextMenuSubject = new BehaviorSubject<IMenu[]>([]);
  contextMenuSubject$: Observable<IMenu[]> =
    this.contextMenuSubject.asObservable();

  private selectedMenu = new BehaviorSubject<string>('');
  selectedMenu$ = this.selectedMenu.asObservable();

  constructor() {}

  setShowSidebar(value: boolean) {
    this.showSidebar.next(value);
  }

  setContextMenu(value: IMenu[]) {
    this.contextMenuSubject.next(value);
  }

  setSelectedMenu(value?: string) {
    if (value) {
      this.selectedMenu.next(value);
    }
  }
}
