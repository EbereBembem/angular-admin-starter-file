import { Injectable } from '@angular/core';
import * as localforage from 'localforage';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  private storage: LocalForage;
  readonly appName = 'appName';

  constructor() {
    this.storage = localforage.createInstance({
      name: this.appName,
      storeName: `${this.appName}_indexDb`,
    });
  }

  setKeyValue(key: string, value: string) {
    this.saveKeyValue(key, value);
  }

  getKeyValue(key: string) {
    return this.fetchKeyValue(key);
  }

  removeKeyValue(key: string) {
    return this.deleteKeyValue(key);
  }

  removeKeyValuePairs(keys: string[]) {
    if (keys?.length) {
      keys.forEach((key) => this.removeKeyValue(key));
    }
  }

  clear() {
    this.clearStorage();
  }

  private saveKeyValue(key: string, value: string) {
    return this.storage.setItem(key, value);
  }

  private fetchKeyValue(key: string) {
    return this.storage.getItem(key);
  }

  private deleteKeyValue(key: string) {
    this.storage.removeItem(key);
  }

  private clearStorage() {
    this.storage.clear();
  }
}
