import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  post<T>(url: string, payload?: any, options?: RequestOptions): Observable<T> {
    payload = {
      ...payload,
    };

    payload = {
      ...payload,
    };

    options = options || {};

    return this.http.post<T>(url, payload, options);
  }

  get(url: string, options?: any): Observable<any> {
    options = { ...options };
    return from(
      this.http
        .get(url, options)
        .toPromise()
        .then((resp) => resp)
    ).pipe(map((data) => data));
  }

  put(url: string, body: any, options?: RequestOptions): Observable<any> {
    options = options || {};
    return from(
      this.http
        .put(url, body, options)
        .toPromise()
        .then((resp) => resp)
    ).pipe(map((data) => data));
  }

  delete(url: string, options?: any): Observable<any> {
    options = { ...options };
    return from(
      this.http
        .delete(url, options)
        .toPromise()
        .then((resp) => resp)
    ).pipe(map((data) => data));
  }
}

interface RequestOptions {
  headers?: any | HttpHeaders;
  params?:
    | HttpParams
    | {
        [param: string]: string | string[];
      };
  reportProgress?: boolean;
  responseType?: 'json';
  withCredentials?: boolean;
}
