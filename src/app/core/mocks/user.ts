import { IAdmin } from '../models/admin';
import { IUser } from '../models/user';

export const USER_LIST: IUser[] = [];
export const USER_DATA: IAdmin = {
  firstName: 'Joe',
  lastName: 'Doe',
  email: 'loeoe@jdjd.com',
};
