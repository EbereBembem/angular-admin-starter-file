import { Location } from '@angular/common';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { isTokenURL } from '../helpers/route-utils';
import { AuthenticationService } from '../security/authentication.service';

@Injectable({ providedIn: 'root' })
export class CredentialInterceptor implements HttpInterceptor {
  constructor(
    private router: Router,
    private location: Location,
    private auth: AuthenticationService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const urlPath = this.location.path();
    if (!isTokenURL(urlPath) && !!urlPath) {
      return next.handle(req);
    } else {
      return of(this.auth.getToken()).pipe(
        switchMap((token) => {
          let headers: any = {};
          if (token) {
            headers['Authorization'] = token;
          }
          req = req.clone({
            setHeaders: headers,
          });
          return next.handle(req);
        }),
        tap(
          () => {},
          (err: HttpErrorResponse | any) => {
            if (!isTokenURL(urlPath)) {
              return;
            }
            if (err instanceof HttpErrorResponse) {
              if (
                (err as HttpErrorResponse).status === 401 ||
                (err as HttpErrorResponse).status === 498
              ) {
                this.auth.logOut();
              }

              if (err.status !== 409) {
                this.router.navigate(['']);
              }
              return;
            }
          }
        )
      );
    }
  }
}
