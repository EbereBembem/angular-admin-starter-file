import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription, Observable, fromEvent, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AlertService } from './core/services/alert.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'ng-project-starter';
  subscription = new Subscription();
  onlineEvent: Observable<Event> = of();
  offlineEvent: Observable<Event> = of();
  constructor(private alert: AlertService, private router: Router) {}

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.onlineEvent = fromEvent(window, 'online');
    this.offlineEvent = fromEvent(window, 'offline');

    this.subscription.add(
      this.onlineEvent.subscribe(() => {
        this.alert.basicAlert(environment.onlineMsg, 'Connectivity', {
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
        });
        this.router.navigate(['/']);
      })
    );

    this.subscription.add(
      this.offlineEvent.subscribe(() => {
        this.alert.basicAlert(environment.offlineMsg, 'Connectivity', {
          verticalPosition: 'bottom',
          horizontalPosition: 'center',
        });
      })
    );
  }
}
