import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollTopComponent } from './scroll-top/scroll-top.component';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [ScrollTopComponent],
  imports: [CommonModule, MatIconModule],
  exports: [ScrollTopComponent],
})
export class ScrollModule {}
