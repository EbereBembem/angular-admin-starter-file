import { MediaMatcher } from '@angular/cdk/layout';
import {
  ChangeDetectorRef,
  Component,
  HostListener,
  OnInit,
} from '@angular/core';

@Component({
  selector: 'app-scroll-top',
  templateUrl: './scroll-top.component.html',
  styleUrls: ['./scroll-top.component.scss'],
})
export class ScrollTopComponent implements OnInit {
  showMobileNav = false;
  isShowScrollTop: boolean = false;
  isFixScroll = false;
  topPosToStartShowing = 100;
  fixBottom = 32;

  constructor(changeRef: ChangeDetectorRef, media: MediaMatcher) {
    const mobileQuery = media.matchMedia('(min-width: 768px)');
    this.mobileQueryListener = () => {
      this.fixBottom = mobileQuery.matches ? 221 : 32;
      this.topPosToStartShowing = mobileQuery.matches ? 200 : 100;
    };
    mobileQuery.addEventListener('change', this.mobileQueryListener);
  }

  private mobileQueryListener: () => void;

  ngOnInit(): void {}

  @HostListener('window:scroll')
  checkScroll() {
    const scrollPosition =
      window.pageYOffset ||
      document.documentElement.scrollTop ||
      document.body.scrollTop ||
      0;
    const bodyHeight = document.body.offsetHeight || 0;
    this.isShowScrollTop = scrollPosition >= this.topPosToStartShowing;
    this.isFixScroll =
      !!bodyHeight &&
      !!scrollPosition &&
      bodyHeight - scrollPosition <= this.fixBottom;
  }

  gotoTop() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  }
}
