import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageSliderPopUpComponent } from './image-slider-pop-up.component';

describe('ImageSliderPopUpComponent', () => {
  let component: ImageSliderPopUpComponent;
  let fixture: ComponentFixture<ImageSliderPopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImageSliderPopUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageSliderPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
