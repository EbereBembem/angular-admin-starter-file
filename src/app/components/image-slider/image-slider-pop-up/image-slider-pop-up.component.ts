import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-image-slider-pop-up',
  templateUrl: './image-slider-pop-up.component.html',
  styleUrls: ['./image-slider-pop-up.component.scss'],
})
export class ImageSliderPopUpComponent implements OnInit {
  imageList: string[] = [];
  selectedImgIndex = 0;
  showImageSlider = false;

  selectedImage!: string;
  constructor(
    public dialogRef: MatDialogRef<ImageSliderPopUpComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    if (this.data) {
      this.imageList =
        (this.data.imageList &&
          this.data.imageList.length &&
          this.data.imageList) ||
        [];
      this.selectedImgIndex = this.data.selectedImgIndex || 0;
      this.showImageSlider =
        this.imageList && !!this.imageList.length && this.imageList.length > 1;
      this.selectedImage = this.imageList[this.selectedImgIndex];
    }
  }

  close() {
    this.dialogRef.close();
  }

  setSelectImage(index: number | any) {
    if (
      this.imageList &&
      this.imageList.length &&
      index < this.imageList.length
    ) {
      this.selectedImgIndex = index;
      this.selectedImage = this.imageList[index];
    }
  }
}
