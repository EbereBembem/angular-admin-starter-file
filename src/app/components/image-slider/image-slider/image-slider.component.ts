import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { combineLatest, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { IMAGE_SLIDER_PREVIEW_MODAL_SETTING } from 'src/app/core/constants/modal-settings';
import { openModal } from 'src/app/core/helpers/modal';
import { ImageSliderService } from 'src/app/core/services/image-slider.service';
import { ImageSliderPopUpComponent } from '../image-slider-pop-up/image-slider-pop-up.component';

@Component({
  selector: 'app-image-slider',
  templateUrl: './image-slider.component.html',
  styleUrls: ['./image-slider.component.scss'],
})
export class ImageSliderComponent implements OnInit {
  @Input() imageDesc: string = '';
  pageReady$!: Observable<any>;

  imageList!: string[];

  showImageSlider = false;

  selectedImage!: string;
  selectedImgIndex!: number;

  constructor(
    public dialog: MatDialog,
    private imageSliderService: ImageSliderService
  ) {}

  ngOnInit(): void {
    this.pageReady$ = combineLatest([
      this.imageSliderService.imageSlider$,
      this.imageSliderService.imageSliderIndex$,
    ]).pipe(
      tap(([images, index]) => {
        this.imageList = images;

        if (this.imageList && this.imageList.length) {
          this.showImageSlider = this.imageList.length > 1;
          this.selectedImgIndex = index || 0;
          this.selectedImage = this.imageList[this.selectedImgIndex];
        }
      })
    );
  }

  setSelectImage(index: number) {
    if (
      this.imageList &&
      this.imageList.length &&
      index < this.imageList.length
    ) {
      this.selectedImgIndex = index;
      this.selectedImage = this.imageList[index];
    }
  }

  preview() {
    const imageList = this.imageList;
    const selectedImgIndex = this.selectedImgIndex;
    const data = {
      imageList,
      selectedImgIndex,
    };

    openModal(this.dialog, ImageSliderPopUpComponent, {
      ...IMAGE_SLIDER_PREVIEW_MODAL_SETTING,
      data,
    });
  }
}
