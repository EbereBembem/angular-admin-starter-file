import { NgModule } from '@angular/core';
import { ImageSliderComponent } from './image-slider/image-slider.component';
import { ImageSliderListComponent } from './image-slider-list/image-slider-list.component';
import { ImageSliderPopUpComponent } from './image-slider-pop-up/image-slider-pop-up.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    ImageSliderComponent,
    ImageSliderListComponent,
    ImageSliderPopUpComponent,
  ],
  imports: [SharedModule],
  exports: [ImageSliderComponent],
})
export class ImageSliderModule {}
