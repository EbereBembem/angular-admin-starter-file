import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageSliderListComponent } from './image-slider-list.component';

describe('ImageSliderListComponent', () => {
  let component: ImageSliderListComponent;
  let fixture: ComponentFixture<ImageSliderListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImageSliderListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageSliderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
