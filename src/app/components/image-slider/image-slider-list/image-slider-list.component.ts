import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  OnDestroy,
} from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-image-slider-list',
  templateUrl: './image-slider-list.component.html',
  styleUrls: ['./image-slider-list.component.scss'],
})
export class ImageSliderListComponent implements OnInit, OnDestroy {
  @Output() selectedImageEmitter = new EventEmitter<number>();
  @Input() imageDesc: string = '';
  @Input() imageList!: string[];
  @Input() selectedImgIndex!: number;

  selectedImageBehSubj = new BehaviorSubject<number | null>(null);
  subscriptions = new Subscription();

  constructor() {
    this.subscriptions.add(
      this.selectedImageBehSubj
        .asObservable()
        .pipe(debounceTime(10), distinctUntilChanged())
        .subscribe((index) => this.updateSelectedImage(index as number))
    );
  }

  ngOnDestroy(): void {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
  }

  ngOnInit(): void {
    if (this.imageList && this.imageList.length) {
      this.selectedImgIndex = this.selectedImgIndex || 0;
      this.setSelectImage(this.selectedImgIndex);
    }
  }

  setSelectImage(index: number) {
    this.selectedImageBehSubj.next(index);
  }

  private updateSelectedImage(index: number) {
    if (
      this.imageList &&
      this.imageList.length &&
      index < this.imageList.length
    ) {
      this.selectedImgIndex = index;
      this.selectedImageEmitter.emit(index);
    }
  }
}
