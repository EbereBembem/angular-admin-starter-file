import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-table-wrapper',
  templateUrl: './table-wrapper.component.html',
  styleUrls: ['./table-wrapper.component.scss'],
})
export class TableWrapperComponent implements OnInit {
  @Input() showHeaderArea = true;
  @Input() showSearchArea = true;

  constructor() {}

  ngOnInit(): void {}
}
