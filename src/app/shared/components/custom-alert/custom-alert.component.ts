import { Component, Inject, OnInit } from '@angular/core';
import {
  MatSnackBarRef,
  MAT_SNACK_BAR_DATA,
} from '@angular/material/snack-bar';
import { IAlertContent } from 'src/app/core/models/alert';

@Component({
  selector: 'app-custom-alert',
  templateUrl: './custom-alert.component.html',
  styleUrls: ['./custom-alert.component.scss'],
})
export class CustomAlertComponent implements OnInit {
  constructor(
    private snackBarRef: MatSnackBarRef<CustomAlertComponent>,
    @Inject(MAT_SNACK_BAR_DATA) public data: IAlertContent
  ) {}

  ngOnInit(): void {}

  close() {
    this.snackBarRef.dismiss();
  }
}
