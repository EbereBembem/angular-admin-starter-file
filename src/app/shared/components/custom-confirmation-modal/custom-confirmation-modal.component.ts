import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IModalConfirmationData } from 'src/app/core/models/modal';

@Component({
  selector: 'app-custom-confirmation-modal',
  templateUrl: './custom-confirmation-modal.component.html',
  styleUrls: ['./custom-confirmation-modal.component.scss'],
})
export class CustomConfirmationModalComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<CustomConfirmationModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IModalConfirmationData
  ) {}

  ngOnInit(): void {}

  cancel() {
    this.closeModal(false);
  }

  confirmation() {
    this.closeModal(true);
  }

  private closeModal(response?: boolean) {
    this.dialogRef.close(response);
  }
}
