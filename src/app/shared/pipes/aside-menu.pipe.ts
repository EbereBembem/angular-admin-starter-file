import { Pipe, PipeTransform } from '@angular/core';
import { IMenu } from 'src/app/core/models/menu';
import { AuthenticationService } from 'src/app/core/security/authentication.service';

@Pipe({
  name: 'asideMenu',
})
export class AsideMenuPipe implements PipeTransform {
  constructor(private auth: AuthenticationService) {}

  transform(menu: IMenu): boolean {
    let result = false;
    const user = this.auth.getCurrentAdmin();

    if (
      menu.hasOwnProperty('isSuperAdminOnly') &&
      menu.isSuperAdminOnly === true
    ) {
      result = user.isSuperAdmin;
    } else if (
      menu.hasOwnProperty('isAdminOnly') &&
      menu.isAdminOnly === true
    ) {
      result = user.isAdmin;
    } else {
      result = true;
    }

    return result;
  }
}
