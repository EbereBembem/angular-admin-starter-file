import { Pipe, PipeTransform } from '@angular/core';
import { IUser } from 'src/app/core/models/user';

@Pipe({
  name: 'users',
})
export class UsersPipe implements PipeTransform {
  transform(userList: IUser[], searchText: string): IUser[] {
    // if (searchText) {
    //   return userList.filter((user) =>
    //     `${user?.firstName}${user?.lastName}`
    //       .toLowerCase()
    //       .includes(searchText.toLowerCase())
    //   );
    // }
    return userList;
  }
}
