import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomMaterialModule } from './custom-material/custom-material.module';
import { EmptyStateComponent } from './components/empty-state/empty-state.component';
import { LoadingStateComponent } from './components/loading-state/loading-state.component';
import { UsersPipe } from './pipes/search/users.pipe';
import { RouterModule } from '@angular/router';
import { ExternalLinksDirective } from './directives/external-links.directive';
import { ImgLazyLoadDirective } from './directives/img-lazy-load.directive';
import { InputFileDirective } from './directives/input-file.directive';
import { ButtonLoadingDirective } from './directives/button-loading.directive';
import { NumbersOnlyInputDirective } from './directives/numbers-only-input.directive';
import { FileDragDropUploadDirective } from './directives/file-drag-drop-upload.directive';
import { AsideMenuPipe } from './pipes/aside-menu.pipe';
import { CustomConfirmationModalComponent } from './components/custom-confirmation-modal/custom-confirmation-modal.component';
import { TableWrapperComponent } from './components/table-wrapper/table-wrapper.component';
import { CustomAlertComponent } from './components/custom-alert/custom-alert.component';
import { SanitizerPipe } from './pipes/sanitizer.pipe';

@NgModule({
  declarations: [
    EmptyStateComponent,
    LoadingStateComponent,
    UsersPipe,
    ExternalLinksDirective,
    ImgLazyLoadDirective,
    InputFileDirective,
    ButtonLoadingDirective,
    NumbersOnlyInputDirective,
    FileDragDropUploadDirective,
    AsideMenuPipe,
    CustomConfirmationModalComponent,
    CustomAlertComponent,
    TableWrapperComponent,
    SanitizerPipe,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CustomMaterialModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    CustomMaterialModule,
    EmptyStateComponent,
    LoadingStateComponent,
    UsersPipe,
    ExternalLinksDirective,
    ImgLazyLoadDirective,
    InputFileDirective,
    ButtonLoadingDirective,
    NumbersOnlyInputDirective,
    FileDragDropUploadDirective,
    AsideMenuPipe,
    CustomConfirmationModalComponent,
    CustomAlertComponent,
    TableWrapperComponent,
    SanitizerPipe,
  ],
})
export class SharedModule {}
