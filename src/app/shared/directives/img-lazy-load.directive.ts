import { DOCUMENT } from '@angular/common';
import {
  Directive,
  ElementRef,
  HostListener,
  Inject,
  Input,
  Renderer2,
} from '@angular/core';
import { APP_FALLBACK_IMAGE } from 'src/app/core/constants/images';

@Directive({
  selector: '[appImgLazyLoad]',
})
export class ImgLazyLoadDirective {
  @Input() showPreloadImg = true;
  supportsLoadingAttr = false;
  imageElement: HTMLImageElement;
  imgContainer: HTMLDivElement | undefined;

  private uniqueClass = 'span-preloader';

  constructor(
    { nativeElement }: ElementRef<HTMLImageElement>,
    private renderer: Renderer2,
    @Inject(DOCUMENT) private document: any
  ) {
    this.supportsLoadingAttr = 'loading' in HTMLImageElement.prototype;
    this.imageElement = nativeElement;
  }

  ngOnInit(): void {
    this.imgContainer = this.imageElement.parentElement as HTMLDivElement;

    if (this.imgContainer) {
      this.imgContainer.style.display = 'grid';
      this.imgContainer.classList.add('pulsate');

      if (this.showPreloadImg) {
        const logoSpan = this.logoContainer();
        this.imgContainer.append(logoSpan);
      }
    }

    if (this.supportsLoadingAttr) {
      this.imageElement.setAttribute('loading', 'lazy');
    }

    this.imageElement.style.gridRow = '1/2';
    this.imageElement.style.gridColumn = '1/2';
    this.imageElement.style.zIndex = '2';
    this.imageElement.style.minHeight = '0';
  }

  @HostListener('load', ['$event'])
  checkLoaded(event?: any) {
    if (this.imgContainer && this.imageElement.complete) {
      this.removePreloader();
    }
  }

  @HostListener('error', ['$event'])
  checkError() {
    this.imageElement.src = APP_FALLBACK_IMAGE;
    this.removePreloader();
  }

  private removePreloader() {
    const span = this.imgContainer?.querySelector(`.${this.uniqueClass}`);
    this.imageElement.removeAttribute('hidden');

    if (span) {
      if (!this.supportsLoadingAttr) {
        span.remove();
      } else {
        setTimeout(() => {
          span.remove();
        }, 4000);
      }
    }
    if (this.imgContainer) {
      this.imgContainer.style.background = 'transparent';
    }
    this.imageElement.removeAttribute('hidden');
  }

  private logoContainer(): HTMLSpanElement {
    const logoSpan: HTMLSpanElement = this.document.createElement('span');
    logoSpan.classList.add('w-100', 'd-flex', this.uniqueClass);
    logoSpan.setAttribute(
      'style',
      'height: 100%; grid-row: 1/2; grid-column: 1/2'
    );

    logoSpan.innerHTML = `logo`;
    return logoSpan;
  }
}
