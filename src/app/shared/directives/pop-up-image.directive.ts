import { Directive, ElementRef, HostListener } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ImageSliderPopUpComponent } from 'src/app/components/image-slider/image-slider-pop-up/image-slider-pop-up.component';
import { IMAGE_SLIDER_PREVIEW_MODAL_SETTING } from 'src/app/core/constants/modal-settings';
import { openModal } from 'src/app/core/helpers/modal';

@Directive({
  selector: '[appPopUpImage]',
})
export class PopUpImageDirective {
  imageElement!: HTMLImageElement;
  imgSrc!: string;

  constructor(
    { nativeElement }: ElementRef<HTMLImageElement>,
    public dialog: MatDialog
  ) {
    this.imageElement = nativeElement;
  }

  ngOnInit() {
    this.imgSrc = this.imageElement?.src;

    if (this.imgSrc) {
      this.imageElement.style.cursor = 'zoom-in';
    }
  }

  @HostListener('click', ['$event'])
  clickImage() {
    if (this.imgSrc) {
      const data = {
        imageList: [this.imgSrc],
      };
      openModal(this.dialog, ImageSliderPopUpComponent, {
        ...IMAGE_SLIDER_PREVIEW_MODAL_SETTING,
        data,
      });
    }
  }
}
