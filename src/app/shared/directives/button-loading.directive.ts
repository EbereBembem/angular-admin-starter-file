import { Directive, ElementRef, Input } from '@angular/core';
import { MatButton } from '@angular/material/button';

@Directive({
  selector: '[appButtonLoading]',
})
export class ButtonLoadingDirective {
  savedText: string | undefined;

  @Input() loadText = 'Processing...';

  @Input() set appButtonLoading(value: boolean) {
    this.toggle(value);
  }

  constructor(private element: ElementRef, private matButton: MatButton) {
    this.toggle(this.appButtonLoading);
  }

  toggle(condition: boolean) {
    if (
      !this.matButton.disabled ||
      this.matButton._elementRef.nativeElement.innerHTML.trim() ===
        this.loadText
    ) {
      condition ? this.show() : this.hide();
    }
  }

  show() {
    this.savedText = this.element.nativeElement.innerHTML;

    if (this.loadText) {
      this.element.nativeElement.innerHTML = this.loadText;
    }

    this.matButton.disabled = true;
  }

  hide() {
    this.matButton.disabled = false;
    if (this.savedText) {
      this.element.nativeElement.innerHTML = this.savedText;
    }
  }
}
