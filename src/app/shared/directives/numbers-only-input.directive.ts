import { Directive, ElementRef, HostListener } from '@angular/core';
import { KEYBOARD_EVENT_KEYS } from 'src/app/core/constants/keyboard-keys';

@Directive({
  selector: '[appNumbersOnlyInput]',
})
export class NumbersOnlyInputDirective {
  inputElem: ElementRef;
  navigationKeys = KEYBOARD_EVENT_KEYS;

  constructor(private elRef: ElementRef) {
    this.inputElem = this.elRef;
  }

  @HostListener('keydown', ['$event'])
  allowOnlyDigitInputs(e: KeyboardEvent) {
    if (
      this.navigationKeys.indexOf(e.key) > -1 ||
      ((e.ctrlKey || e.metaKey) && e.key == 'v')
    ) {
      return;
    }

    // Ensure that it is a number and stop the keypress
    if (e.key === ' ' || isNaN(Number(e.key))) {
      e.preventDefault();
    }
  }

  @HostListener('paste', ['$event'])
  allowPasteDigits(event: ClipboardEvent) {
    const clipboardData = event.clipboardData;
    const pastedText = clipboardData?.getData('text');
    const onlyReg = /^[0-9]{0,}$/;

    if (!!pastedText && onlyReg.test(pastedText)) {
      return;
    } else {
      event.preventDefault();
    }
  }
}
