import {
  Directive,
  ElementRef,
  EventEmitter,
  HostBinding,
  HostListener,
  Output,
} from '@angular/core';

@Directive({
  selector: '[appFileDragDropUpload]',
})
export class FileDragDropUploadDirective {
  @Output() private filesChangeEmiter: EventEmitter<File[]> =
    new EventEmitter();

  @HostBinding('style.background') private background: string | any;
  initalBackground = '#fff';

  constructor(private element: ElementRef) {
    this.initalBackground = window.getComputedStyle(
      this.element.nativeElement
    ).backgroundColor;
  }

  @HostListener('dragover', ['$event']) public onDragOver(event: any) {
    event.preventDefault();
    event.stopPropagation();

    this.background = 'lightgray';
  }

  @HostListener('dragleave', ['$event']) public onDragLeave(event: any) {
    event.preventDefault();
    event.stopPropagation();
    this.background = this.initalBackground;
  }

  @HostListener('drop', ['$event']) public onDrop(event: any) {
    event.preventDefault();
    event.stopPropagation();
    this.background = this.initalBackground;

    let files = event.dataTransfer.files;
    this.filesChangeEmiter.emit(files);
  }
}
