import {
  Directive,
  ElementRef,
  EventEmitter,
  HostListener,
  Output,
} from '@angular/core';
import { MAX_INPUT_FILE_UPLOAD_MB } from 'src/app/core/constants/number-constants';
import { EXCEEDS_MAX_UPLOAD_FILE_SIZE_MSG } from 'src/app/core/constants/text-utils';
import { AlertService } from 'src/app/core/services/alert.service';

@Directive({
  selector: '[appInputFile]',
})
export class InputFileDirective {
  readonly byteSize = 1024;
  readonly maxSize = MAX_INPUT_FILE_UPLOAD_MB * this.byteSize * this.byteSize;
  @Output() fileEmiter: EventEmitter<File | null> = new EventEmitter();

  inputElement: HTMLInputElement;
  file: File | undefined;

  constructor(
    private alertService: AlertService,
    { nativeElement }: ElementRef
  ) {
    this.inputElement = nativeElement;
  }

  @HostListener('change', ['$event']) onChange = (event: any) => {
    event.preventDefault();
    this.file =
      event?.target?.files &&
      event?.target?.files.length &&
      event?.target?.files[0];
    const isValid = (this.file && this.file.size < this.maxSize) || false;
    if (!isValid) {
      this.fileEmiter.emit(null);
      return this.alertService.showError(EXCEEDS_MAX_UPLOAD_FILE_SIZE_MSG);
    }
    if (this.inputElement?.accept) {
      const CRITERIA = this.inputElement?.accept
        .split(',')
        .filter((criterion) => !!criterion)
        .map((criterion) =>
          criterion.toLocaleLowerCase().replace('.', '').trim()
        );

      if (!CRITERIA.some(this.containsFileExtension.bind(this))) {
        return this.alertService.showError('Invalid File Type');
      }
    }
    this.fileEmiter.emit(this.file);
    return;
  };

  private containsFileExtension(type: string) {
    const fileType = this.file?.type.trim();
    let result = false;
    if (fileType) {
      result = fileType.includes(type) || type.includes(fileType);
    }
    return result;
  }
}
