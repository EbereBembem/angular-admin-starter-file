export const environment = {
  production: true,
  baseAPI: '',
  onlineMsg: 'You are back online',
  offlineMsg: 'You are offline',
};
